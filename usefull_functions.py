#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 12:58:16 2018

@author: ecutolo
"""
import pyroms,netCDF4,sys
import numpy as np
import xarray as xr
from datetime import datetime, timedelta
import cartopy.crs as ccrs

import matplotlib.pyplot as plt
import matplotlib.patches as patches

from matplotlib.animation import FuncAnimation
from wmop_basic import *

output_dir = "/mnt/waverider/data/WMOP/WMOP_HINDCAST/Outputs"
wmop_preswot_dir = "/home/modelling/data/workdir_2/Eugenio/run/Outputs/PRESWOT{0}/"
wmop_preswot_input_dir = "/home/modelling/data/workdir_2/Eugenio/run/Inputs/Surface/"

def TKE_extract(var_name,data,extra_arg):
	mean_data = extra_arg
	fluctuations_value = (np.mean((data[2]-mean_data[2])**2)+np.mean((data[1]-mean_data[1])**2))*0.5
	fluctuations_std = (np.std((data[2]-mean_data[2])**2)+np.std((data[1]-mean_data[1])**2))*0.5
	return [fluctuations_value,fluctuations_std]

def compute_vorticity(ds):
	pm = pyroms.utility.move2grid(ds.pm,'rho','psi')
	pn = pyroms.utility.move2grid(ds.pn,'rho','psi')
	DDxdiff = (pm**-1)
	DDydiff = (pn**-1)
		
	u = pyroms.utility.move2grid(ds.u,'u','psi')
	v = pyroms.utility.move2grid(ds.v,'v','psi')
			
	UUX,UUY = np.gradient(u)
	VVX,VVY = np.gradient(v);
			
	omega=7.2921E-5
	lat_2d = pyroms.utility.move2grid(ds.lat_rho,'rho','psi')
	f_matrix=2*omega*np.sin(np.deg2rad(lat_2d))
			
	vorticity=(VVX/(DDxdiff)-UUY/(DDydiff))/f_matrix;
	return vorticity

def wmop_preswot_file(params,date):
	nesting, file_type = params
	wmop_dir = wmop_preswot_dir.format(nesting)

	rd = datetime.strptime(date,"%d/%m/%Y").timetuple()
	filename =  "{0}/{1}/{2:02}/{3:02}/roms_WMOP_HINDCAST_{1}{2:02}{3:02}_{4}.nc".format(wmop_dir,rd[0],rd[1],rd[2],file_type)
	if os.path.exists(filename) or 'dont_check_existence' in params:
		return filename
	else:
		return False
	
def wmop_preswot_hirlam_file(params,date):
	nesting, file_type = params
	rd = datetime.strptime(date,"%d/%m/%Y").timetuple()
	filename =  "{0}/roms_WMOPv2.3{4}_hirlam_HINDCAST_frc_{1}{2:02}{3:02}.nc".format(wmop_preswot_input_dir,rd[0],rd[1],rd[2],file_type)
	if os.path.exists(filename) or 'dont_check_existence' in params:
		return filename
	else:
		return False

def plot_spec(kspec,pspec,title):
    global k,s5,s3,s113
    fig = plt.figure(figsize=(15,4))
    plt.suptitle(title,size = 16,y=1.05)
    
    axis1 = fig.add_subplot(141)
    plt.loglog(k,s5,'k-',label=r'$k^{-5}$')
    plt.loglog(k,s4,'k-.',label=r'$k^{-4}$')
    plt.loglog(k,s113,'k--',label=r'$k^{-11/3}$')
    plt.loglog(kspec,pspec)
    plt.xlim(1E-6,1E-3)
    plt.ylim(1E-8,1E5)
    plt.xlabel('cpm')
    plt.ylabel('$m^2$/cpm')
    plt.title('SSH Spectra')
    plt.legend()
    def tick_function(X):
        W = 1. / (X)/1000
        return ["%.3f" % z for z in W]
    axis2 = fig.add_subplot(142)
    pp.plot_spectrum(kspec,pspec,1*1E3,10*1E3)
    plt.title('Slope : 3km - 10km')
    axis2.set_xticklabels(tick_function(axis2.get_xticks()))
    #axis2.set_xlim(2,10)
    axis3 = fig.add_subplot(143)
    pp.plot_spectrum(kspec,pspec,10*1E3,50*1E3)
    plt.title('Slope : 10km - 50km')
    axis3.set_xticklabels(tick_function(axis3.get_xticks()))
    axis4 = fig.add_subplot(144)
    pp.plot_spectrum(kspec,pspec,50*1E3,150*1E3)
    plt.title('Slope : 50km - 250km')
    axis3.set_xticklabels(tick_function(axis3.get_xticks()))

def plot_spec2(kspec,pspec,label=None,use_ax=None):
	if not use_ax:
		fig = plt.figure(figsize=(10, 10))
		ax1 = fig.add_subplot(111)
	else:
		ax1 = use_ax
	ax2 = ax1.twiny()
	ax1.loglog(kspec, pspec,label=label)    
	ax1.grid(True)
	ax1.legend()

    #plt.ylim([1e-7, 1e4])
	ax1.set_xlabel('Wavenumber')
	ax1.set_ylabel('Power Spectral Density')
	wl = [150,25,10,5,1,0.5]
	ax1.set_xticks(1/(np.array(wl)*1000))
	ax1Ticks = ax1.get_xticks()
   
	ax2Ticks = ax1Ticks

	def tick_function(X):
		W = 1. / (X)/1000
		return ["%.3f" % z for z in W]

	ax2.set_xscale('log')
	ax2.set_xticks(ax2Ticks)
	ax2.set_xbound(ax1.get_xbound())
	ax2.set_xticklabels(tick_function(ax2Ticks))
	ax2.set_xlabel("Wavelength")
	ax2.grid(b=True, which='minor', color='w', linestyle='--')
	if not use_ax:
		plt.show()
	return ax1
	
def select_roms_dataset_region(dataset,region_limits=False,region_name=False):
	if not(region_limits or region_name):
		raise ValueError('A very specific bad thing happened.')
	elif region_name in available_zones:
		region_limits = available_zones[region_name][0]
	else:
		region_limits = region_limits[0]
	lat = dataset.lat_rho[:,0]
	lon = dataset.lon_rho[0,:]
	mask_rho_lat = np.logical_and(lat>region_limits[2],lat<region_limits[3]).values
	mask_rho_lon = np.logical_and(lon>region_limits[0],lon<region_limits[1]).values
	mask_u_lat = mask_rho_lat
	mask_u_lon = np.logical_and(mask_rho_lon[1:],mask_rho_lon[:-1])
	mask_v_lon = mask_rho_lon
	mask_v_lat = np.logical_and(mask_rho_lat[1:],mask_rho_lat[:-1])
	mask_psi_lat = np.logical_and(mask_rho_lat[1:],mask_rho_lat[:-1])
	mask_psi_lon = np.logical_and(mask_rho_lon[1:],mask_rho_lon[:-1])
	data = dataset.isel(eta_rho=mask_rho_lat,xi_rho=mask_rho_lon)
	data = data.isel(eta_u=mask_u_lat,xi_u=mask_u_lon)
	data = data.isel(eta_v=mask_v_lat,xi_v=mask_v_lon)
	data = data.isel(eta_psi=mask_psi_lat,xi_psi=mask_psi_lon)
	return data

import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

def draw_box(ax,region_name):
    nested_zone_limits = available_zones[region_name][0]
    nested_dim = (abs(nested_zone_limits[1]-nested_zone_limits[0]),abs(nested_zone_limits[3]-nested_zone_limits[2]))
    ax.add_patch(patches.Rectangle((nested_zone_limits[0], nested_zone_limits[2]), nested_dim[0], nested_dim[1],fill=False,transform=ccrs.PlateCarree()))


def draw_background_map(zone,zone_limits=False,ax=False,drawy=True,figsize=[10,8],subplotn=111,fig=False,grid_line=True,coast_line=True,draw_logo=False):
    
    if not fig and ax is False:
        fig = plt.figure(figsize=figsize)
    
    #ax = plt.axes(projection=ccrs.Mercator());
    
    if not ax:
        proj = ccrs.Mercator()
        ax = fig.add_subplot(subplotn, projection=proj)
    
    if not zone_limits:
        zone_limits = available_zones[zone][0]
        logo_position = available_zones[zone][1]
    ax.set_extent([zone_limits[0], zone_limits[1], zone_limits[2], zone_limits[3]], crs=ccrs.PlateCarree())
    ax.set_aspect('auto')
    if coast_line:
        ax.add_feature(cfeature.GSHHSFeature(scale='h',facecolor='gray'))
    if grid_line:
        gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.1)
        gl.xlabels_top = False
        gl.ylabels_right = False
        gl.ylabels_left = drawy
        gl.xlines = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.xlabel_style = {'size': 15}
        gl.ylabel_style = {'size': 15}
    if draw_logo:
        original_ax = ax#plt.gca()
        logo = mpl.image.imread(logo_path)
        newax = fig.add_axes([0.67, 0.20, 0.06, 0.2], anchor='SE', zorder=1)
        newax.imshow(logo)
        newax.axis('off')			
        plt.sca(original_ax)
    return fig,ax
