from datetime import datetime

nesting_region = 'PRE-SWOT'

wmop_base_dir = '/mnt/waverider/data/WMOP/'

N       = 32
Vtrans  = 2
theta_s = 7
theta_b = 0.2
Tcline  = 60

default_chunk = {'s_rho':4,'ocean_time':30}

wmop_output_files = {'WMOP2.0_FORECAST': wmop_base_dir+'WMOP_FORECAST/Outputs/FORECAST_MFS/forecast_scratch/{0}_{1}.nc',
                     'WMOP2.0_FORECAST_ASSIM': wmop_base_dir+'WMOP_FORECAST/Outputs/FORECAST_MFS_ASSIM/forecast_scratch/{0}_{1}.nc',
                     'WMOP3.0_FORECAST': wmop_base_dir+'WMOP_FORECAST_preop/Outputs/FORECAST_MFS_WMOPv3.0/forecast_scratch/{0}_{1}.nc',
                     'WMOP3.0_NESTING_PRESWOT_D': wmop_base_dir+'WMOP_FORECAST_preop/Outputs/FORECAST_MFS_WMOPv3.0_PRE-SWOT_nesting/forecast_scratch/{0}_{1}.nc',
                     'WMOP3.0_NESTING_PRESWOT_R': wmop_base_dir+'WMOP_FORECAST_preop/Outputs/FORECAST_MFS_WMOPv3.0_PRE-SWOT_nesting/forecast_scratch/{0}_nest5_{1}.nc',
                     'WMOP3.0_NESTING_CALYPSO_D':'/local/ecutolo/WORKDIR/WMOP_GRIDS/roms_grd_WMOPv3.0_calypso_nesting5_final.nc'}

#--------------------------- END OF GRID PARAMETERS---------------------------

wmop_preloaded_grid = {'WMOP2.0':'/home/ecutolo/WORKDIR/wmop_data/roms_grd_WMOPv2.0.nc',
					   'WMOP2.3':'/home/ecutolo/WORKDIR/WMOP_GRIDS/roms_grd_WMOPv2.3_final.nc',
					   'WMOP3.0':'/home/eugenio/WORKDIR/WMOP_GRIDS/roms_grd_WMOPv3.0_final.nc',
					   'WMOP_NESTED_BI':'/local/ecutolo/WORKDIR/WMOP_GRIDS/roms_grd_WMOPv2.3_nested_hd_smooth.nc',
					   'WMOP_NESTED_PRESWOT3':'/local/ecutolo/WORKDIR/WMOP_GRIDS/roms_grd_WMOPv3.0_preswot_nesting3_final.nc',
					   'WMOP_NESTED_PRESWOT5':'/home/ecutolo/WORKDIR/WMOP_GRIDS/roms_grd_WMOPv3.0_preswot_nesting5_final.nc',
					   'WMOP_NESTED_CALYPSO5':'/local/ecutolo/WORKDIR/WMOP_GRIDS/roms_grd_WMOPv3.0_calypso_nesting5_final.nc'}

wmop_hindcast_output_dir = "/mnt/waverider/data/WMOP/WMOP_HINDCAST/Outputs/HINDCAST_MFS_{0}"
wmop_forecast_output_dir = "/mnt/waverider/data/WMOP/WMOP_FORECAST/Outputs/FORECAST_MFS{0}"

wmop_inputs_dir = "/mnt/waverider/data/WMOP/WMOP_HINDCAST/Inputs{0}"
wmop_files = ['base_file','frc_file_01','bry_file']
wmop_var_units = {'vorticity':'','rvorticity_bar':'','Pair':'Hpa','wind':'m/s','h':'m','tke':'m2/s2','temp':'C','salt':'psu','u':'m/s','v':'m/s','w':'m/day','ubar':'m/s','vbar':'m/s','zeta':'m','uvz':'m','uvz_geostrophic':'m','rvorticity_bar':'s-1'}
wmop_var_names = {'vorticity':'','rvorticity_bar':'','Pair':'Atm. Pressure','wind':'Wind','h':'Bathymetry','tke':'Turbolent Kinetic Energy','temp':'Temperature','salt':'Salinity','u':'U','v':'V','w':'w','ubar':'m/s','vbar':'m/s','zeta':'Sea Level','uvz':'Circulation','uvz_geostrophic':' Geostrophic Circulation','rvorticity_bar':'Vorticty'}

accepted_date_formats = ('%Y-%m-%d', '%d/%m/%Y')
		
logo_path = '/local/ecutolo/WORKDIR/pyWMOP/logo-Socib_HR.png'
rt_colormap_file = '/LOCALDATA/ecutolo/Matlab/Tools/rt_colormaps.mat'
ts_colors = ['b','r','g','k']

refDate = datetime(1968,5,23)

topography_filename = "/local/ecutolo/WORKDIR/WMOP_GRIDS/MED_GEBCO_30sec.nc"

available_zones = {
		'WesternMed':[[-5.8, 9.2, 34.9, 44.55],6,0.3],
		'AlboranSea':[[-5.8, 0, 34.9, 37.8],3,0.1],
		'BalearicIslands':[[-1, 5.5, 37.8, 41.5],4,0.2],
		'BalearicIslands2':[[1, 5, 38, 40],4,0.2],
		'PRE-SWOT':[[1.805,4.68,38.047,39.6],2,0.2],
		'MallorcaMenorca':[[2, 4.5, 38.8, 40.5],3,0.15],
		'IbizaFormentera':[[0.7, 2, 38.4, 39.5],2,0.1],
		'IbizaChannel':[[-0.2, 2, 38.1, 39.5],3,0.13],
		'MallorcaChannel':[[1.4, 3.6, 38.6, 40.1],3,0.13],
		'ALBOREX2014':[[-1, 1, 36.8, 38.0],1,0.1],
		'AlboranWestAlgerian':[[-3.8, 3, 34.9, 39],3,0.15],
		'EastAlgerian':[[2.9, 9.2, 36.2, 39.5],2,0.2],
		'GulfLion':[[2.2, 6 ,41.5, 43.8],3,0.1],
		'REP14-MED':[[6.5, 8.7, 38.5, 41],1,0.1],
		'Ligurian':[[4, 9.2, 41, 44.55],2,0.2],
		'SardiniaChannel':[[8, 9.2, 36.8, 39.2],2,0.2],
		'CALYPSO':[[-2.5,0,35.5,37],2,0.2],
		'CALYPSO2':[[-3.5,-1.5,35.08,36.87],2,0.2]
	}

def get_wmop_output_dataset(version_name,date_suffix='*[0-9][0-9]',file_type='avg',region_name=None,chunks=default_chunk):
    import xarray as xr
    files = wmop_output_files[version_name].format(date_suffix,file_type)
    print(files)
    ds = xr.open_mfdataset(files,chunks=chunks)
    return ds